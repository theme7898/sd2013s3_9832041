interface Colorable{}

class PlainText implements Colorable{}

interface Changeable extends Colorable{}

class Circle implements Changeable{}

class CompoundText extends PlainText{}


