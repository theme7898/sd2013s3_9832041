import java.util.Date;

class SD2013s3_9832041_Tester{
	public static void main(String[] args){
		//ch2main();
		//ch2_1_main();
		//ch2_5_main();
		//ch2_6_main();
		//ch2_7_main();
		ch2_8_main();
		ch2_9_main();
	}
	/*
	private static void ch2main(){
		System.out.println("---------ch2 person---------");
		Person fp = new Person("임정섭",new Date(0));
		String rName = fp.getName();
		System.out.println(rName);
	}
	*/

	private static void ch2_9_main(){
		System.out.println("--- ch2_9 Override --");
		
		Object3 o = new Object3();
		Automobile3 auto = new Automobile3();
		Object autoObject = new Automoble3();

		auto.equals3(o);//3
		auto.equals3(auto);//2
		auto.equals3(autoObject);//3
		o.equals3(o);//1
		o.equals3(auto);//1
		o.eqauls3(autoObject);//1
		autoObject.equals3(o);//3
		autoObject.equals3(auto);//3
		autoObject.equals3(autoObject);//3
	}
}
	
	private static void ch2_8_main(){
		Automobile[] fleet = new Automobile[3];
		fleet[0] = new Sedan(5);
		fleet[1] = new Minivan(7);
		fleet[2] = new SportsCar(2);

		// version 2는??
		//version 1///////////////////////

		int totalCapacity = 0;

		for (int i = 0; i < fleet.length; i++) {
  			if(fleet[i] instanceof Sedan)
				totalCapacity += fleet[i].getCapacity();
			else if(fleet[i] instanceof Minivan)
				totalCapacity += fleet[i].getCapacity();
			else if(fleet[i] instanceof SportsCar)
				totalCapacity += fleet[i].getCapacity();
		}

		System.out.println("-----ch2_8 Overload ----");
		
		Object2 o = new Object2();
		Automobile2 auto =  new Automobile2();
		Object2 autoObject = new Automobile2();
		
		auto.equals2(o);
		auto.eauals2(auto);
		auto.equals2(autoObject);
		System.out.println("==========================");
		o.equals2(o);
		o.equals2(auto);
		o.equals2(autoObject);
		autoObjetc.equals(o);
		autoObject.equals(auto);
		autoObject.equals(autoObject);
	}
	
	/*

	private static void ch2_7_main(){
		Driver d = new Driver();
		d.moveCar(new Sedan());
		d.moveCar(new MiniVan());
		d.moveCar(new SportsCar());	
	}
	
	private static void ch2_6_main(){
		System.out.println("interface");
		//Colorable c = new Colorable();
		Colorable pt = new PlainText();
		Colorable ct = new CompoundText();
		Colorable cc = new Circle();

		Changeable cc3 = new Circle();

		//Colorable cr4 = new Changeable();
	}

	private static void ch2_5_main(){
		Account ac = new Account();
		Account ca = new CheckingAccount();
		Account cla = new CreditLineAccount();
		Account ctca = new CheckingTrafficCardAccount();

		//CheckingAccount ac2 = new Account();
		//...
	}

	private static void ch2_1_main() {
		System.out.println("----------ch2_1 interface----------");
		SimpleRunner sr = new SimpleRunner();
		sr.run();
		sr.sleep();

		Runnable rn = new SimpleRunner();
		rn.run();
	}
	*/
}

