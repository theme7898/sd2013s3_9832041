public class PolygonData
{
	private String name;
	private Point center; //point class를 확인할수 없다는 오류가 뜨네요.
	
	public PolygonData(String n, Point c)
	{ 
		name = n; 
		center = c; 
	}

	public String getName() { return name; }
	public Point getCenter() { return center; }
}

