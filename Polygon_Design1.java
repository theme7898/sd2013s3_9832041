
import java.awt.*;
import javax.swing.*;
import java.util.*;

class Polygon_Design1 {    
    public static void main(String[] args) {
        JFrame frame = new JFrame("Polygon_Design1");
        frame.setLocation(500, 500);
        frame.setPreferredSize(new Dimension(400, 300));
        Container contentPane = frame.getContentPane();
        Drawing drawingPanel = new Drawing();
        contentPane.add(drawingPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

class Drawing extends JPanel {
    public void paint(Graphics g) {
        ArrayList<String> polygonNames = new ArrayList<String>();
        ArrayList<Point> centerPoints = new ArrayList<Point>();

        polygonNames.add("square");
            polygonNames.add("triangle");
            polygonNames.add("rectangle");
        centerPoints.add(new Point(30,30));
            centerPoints.add(new Point(150, 150));
        centerPoints.add(new Point(200,100));

        for (int i = 0; i < polygonNames.size(); i++) {
            String currentPolygon = polygonNames.get(i);
            Point currentCenter = centerPoints.get(i);
            if (currentPolygon.equals("square"))
                g.drawRect(currentCenter.x -10, currentCenter.y -10, 20, 20);
            else if (currentPolygon.equals("triangle")) {
                g.drawLine(currentCenter.x, currentCenter.y-10,                                            currentCenter.x-10, currentCenter.y+10);
                g.drawLine(currentCenter.x-10, currentCenter.y+10, 
                           currentCenter.x+10, currentCenter.y+10);
                g.drawLine(currentCenter.x+10, currentCenter.y+10,                                            currentCenter.x, currentCenter.y-10);
            } else if (currentPolygon.equals("rectangle"))
                g.drawRect(currentCenter.x -20, currentCenter.y -10, 40, 20);
        }
    }
}



